package com.despegar.broker.repository;

import java.util.Optional;

import com.despegar.broker.domain.entity.Stock;

public interface StocksRepository {

    /*
     * Busca una acción con el ticker dado
     */
    Optional<Stock> find(String ticker); // o retorno null?

    /*
     * Guarda un nueva Acción en caso de no existir, sino lo reemplaza.
     */
    Stock save(Stock stock);
}
