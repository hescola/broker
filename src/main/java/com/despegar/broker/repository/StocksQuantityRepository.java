package com.despegar.broker.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.transaction.TransactionException;

import com.despegar.broker.domain.entity.StockQuantity;

/**
 * Repositorio con Stock de Acciones a las cuales llame StocksQuantity porque StocksStock hubiera
 * sido más confuso
 * 
 * @author hescola
 */
public interface StocksQuantityRepository {

    /**
     * Decrementa el stock de cada accion por la cantidad pasada si es posible satisfacer esa
     * cantidad para todos los casos
     * 
     * @return true si hay suficiente stock para decrementar
     */
    boolean decreaseAllBy(List<StockQuantity> quititiesToDecrement) throws TransactionException;

    /*
     * Agrega o incrementa el stock de la accion segun la cantidad especificada
     */
    StockQuantity addOrIncreaseBy(StockQuantity stockQuantity);

    /*
     * Obtiene el stock de una acción en el momento de ser consultado
     */
    Optional<StockQuantity> find(String ticker);
}
