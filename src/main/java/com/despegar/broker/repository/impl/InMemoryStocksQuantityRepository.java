package com.despegar.broker.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.repository.StocksQuantityRepository;

/**
 * Repositorio en memoria que contiene las cantidades de Stock por ticker de Acciones
 * Tambien es el responsable de asegurar de que las operaciones se produzcan algún efecto
 * colateral no deseado
 * 
 * @author Hernan Adriel Escola
 */
@Singleton
@Repository

public class InMemoryStocksQuantityRepository implements StocksQuantityRepository {

    /**
     * No utilicé una implementación que suporte concurrencia porque se controla en los accesos al
     * mapa
     */
    private Map<String, StockQuantity> quantityAvailableByTicker = new HashMap<>();

    private ReentrantLock lock = new ReentrantLock();

    @Autowired
    public InMemoryStocksQuantityRepository() {
        super();
    }

    /**
     * Este metodo garantiza que no quede inconsistente en el stock de las acciones debido a
     * concurrencia entre la lectura y escritura de los valores. Siempre se incrementa o disminuye
     * los valores adiquierdo un lock. Estas operaciones trabajan syncronizadas
     * Tambien garantiza que aunque se interrumpa el thread o la operacion en medio de la
     * ejecucion, no se vea impactado.
     * Otra opcion era utilizar lock al nivel de la BD, utilizando Spring Data para JPA y definiendo
     * el método del service como Transactional con Isonlation Level = Serializable
     * Lo que haría sin duda es pasar todo a reactive y utilizar Spring Data Reactive con MongoDB
     */
    @Override
    public boolean decreaseAllBy(List<StockQuantity> purchaseOrder) {
        try {
            lock.lock();
            if (canDecreaseAll(purchaseOrder)) {
                decrementStocksByAmount(purchaseOrder);
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

    private boolean canDecreaseAll(List<StockQuantity> decrementChunk) {
        return decrementChunk
            .stream()
            .allMatch(stockQuantity -> hasQuantityGreaterOrEqualsThan(stockQuantity));
    }

    public boolean hasQuantityGreaterOrEqualsThan(StockQuantity toDecrementByTicker) {
        return toDecrementByTicker
            .getAmount() <= find(toDecrementByTicker.getTicker())
                .orElse(StockQuantity.createWithZero(toDecrementByTicker.getTicker()))
                .getAmount();
    }

    private void decrementStocksByAmount(List<StockQuantity> stocksToBuy) {
        Map<String, StockQuantity> amountToDecrementMapped = stocksToBuy.stream()
            .collect(Collectors
                .toMap(stockAmount -> stockAmount.getTicker(), stock -> stock));

        Map<String, StockQuantity> newStocksAmount = quantityAvailableByTicker.entrySet()
            .stream()
            .map(entry -> getDecreasedStockQuantity(entry, amountToDecrementMapped))
            .collect(Collectors.toMap(stockAmount -> stockAmount.getTicker(),
                    stockAmount -> stockAmount));

        commitQuantityAvailableByTicker(newStocksAmount);
    }

    private StockQuantity getDecreasedStockQuantity(Entry<String, StockQuantity> entry,
            Map<String, StockQuantity> amountToDecrementMapped) {
        StockQuantity currentStockQuantity = entry.getValue();
        return currentStockQuantity.subtract(amountToDecrementMapped
            .getOrDefault(currentStockQuantity.getTicker(), currentStockQuantity.withAmount(0)));
    }

    /**
     * Es mas vale simbólico, realmente no existe una transacccion.
     * Si en un hipotetico caso por algun motivo tira alguna Exception algo en el medio del
     * proceso
     * de decremento de stock, quedaria incosistente, por lo que genero una copia y luego la
     * impacto.
     * 
     * @param newStocksQuantityAvailable
     */
    private void commitQuantityAvailableByTicker(Map<String, StockQuantity> newStocksQuantityAvailable) {
        quantityAvailableByTicker = newStocksQuantityAvailable;
    }

    @Override
    public Optional<StockQuantity> find(String ticker) {
        return Optional.ofNullable(quantityAvailableByTicker.get(ticker));
    }

    @Override
    public StockQuantity addOrIncreaseBy(StockQuantity stockQuantity) {
        try {
            lock.lock();
            return this.quantityAvailableByTicker.merge(stockQuantity.getTicker(), stockQuantity,
                    (current, toAdd) -> current.add(toAdd));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

    // public class

    // List<String> stocksWithoutStock = purchaseOrder
    // .stream()
    // .filter(ticker -> stocksToBuy.get(ticker.getTicker()) >
    // stocksStock.getOrDefault(ticker, 0))
    // .collect(Collectors.toList());
    // var haveStockForAllStocks = stocksWithoutStock.size() == 0;
    // y hasta podria retornar info sobre el stock faltante.
}
