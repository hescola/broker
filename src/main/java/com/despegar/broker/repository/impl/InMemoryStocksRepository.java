package com.despegar.broker.repository.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Singleton;

import org.springframework.stereotype.Repository;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.repository.StocksRepository;

@Repository
@Singleton
public class InMemoryStocksRepository implements StocksRepository {

    Map<String, Stock> stocks = new HashMap<>();

    public InMemoryStocksRepository() {
        super();
    }

    public Stock save(Stock stock) {
        return stocks.put(stock.getTicker(), stock);
    }

    @Override
    public Optional<Stock> find(String ticker) {
        return Optional.ofNullable(stocks.get(ticker));
    }

}
