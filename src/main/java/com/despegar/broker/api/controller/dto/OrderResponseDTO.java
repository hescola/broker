package com.despegar.broker.api.controller.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import groovy.transform.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@JsonSerialize
@EqualsAndHashCode
@Data
@ApiModel("Respuesta de la operación de compra de acciones")
public class OrderResponseDTO {

    @JsonSerialize
    public static enum Status
    {
        SUCCESS,
        FAIL
    }

    @ApiModelProperty(notes = "El importe sobrante de la operación en caso de ser existosa")
    private BigDecimal moneyNotExpended;

    @ApiModelProperty(
            notes = "El estado de la operacion: SUCCESS si se realizó correctamente o FAIL si hubo algún error")
    private Status status;

    @ApiModelProperty(
            notes = "Mensaje referido al resultado de la operación por el que no se pudo realizar la operación. "
                    + "Evalué la posibilidad de tener una estructura que retorne una lista de mensajes y podría "
                    + "también ser retornada con un Reponse Status Code y retornar otro tipo de estrxutura para los errores.")
    private String message;

    public static OrderResponseDTO createFailWithReason(String message) {
        return OrderResponseDTO.builder()
            .status(Status.FAIL)
            .moneyNotExpended(null)
            .message(message)
            .build();
    }

}
