package com.despegar.broker.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.despegar.broker.api.controller.dto.OrderItemDTO;
import com.despegar.broker.api.controller.dto.OrderResponseDTO;
import com.despegar.broker.api.controller.dto.StockInformationDTO;
import com.despegar.broker.domain.converter.ModelMapper;
import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.domain.valueObject.OrderResponse;
import com.despegar.broker.domain.valueObject.StockInformation;
import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.exception.NotExistsStockException;
import com.despegar.broker.service.StocksService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Api Controller para las requerimientos solicitados en el ejercicio
 * TODO: Modificar response
 * 
 * @author hescola
 */
@RestController
@RequestMapping("/stocks")
@Api(value = "Api para consulta de información de Acciones y compra de las mismas")
public class StocksController {

    @Autowired
    private StocksService stocksService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/{ticker}")
    @ApiOperation(
            value = "Consulta de información de una acción y su cotización actual a partir de su Ticker (identificador)",
            response = StockInformationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Stock not found for given Ticker"),
            @ApiResponse(code = 503, message = "Operation currently not available"),
    })
    public StockInformationDTO getStockInformation(@PathVariable String ticker)
            throws NotExistsStockException, NotAvailableException {
        StockInformation information = stocksService.getInformation(ticker);
        return modelMapper.map(information, StockInformationDTO.class);
    }

    @PostMapping("/buy")
    @ApiOperation(
            value = "Solicitud de compra de acciones indicando monto a gastar por acción",
            response = OrderResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200,
                    message = "OK (La operación fue procesada correctamente pero puede no haber sido satisfactoria)"),
            @ApiResponse(code = 503, message = "Operation currently not available"),
    })
    public OrderResponseDTO buyStocks(@RequestBody List<OrderItemDTO> orderItems)
            throws NotAvailableException {
        OrderResponse response = stocksService.processPurchaseOrder(modelMapper.map(orderItems, OrderItem.class));
        return modelMapper.map(response, OrderResponseDTO.class);
    }
}
