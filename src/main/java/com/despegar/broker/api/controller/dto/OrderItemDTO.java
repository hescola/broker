package com.despegar.broker.api.controller.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@JsonSerialize
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("Item de la order de compra de acciones")
public class OrderItemDTO {

    @ApiModelProperty(notes = "identificador de la acción a comprar", required = true)
    private String ticker;

    @ApiModelProperty(notes = "Monto máximo a gastar en la operación para esta acción", required = true)
    private BigDecimal money;
}
