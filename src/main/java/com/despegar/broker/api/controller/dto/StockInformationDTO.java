package com.despegar.broker.api.controller.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@EqualsAndHashCode
@JsonSerialize
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("Información actual de una Acción")
@Data
public class StockInformationDTO {

    @ApiModelProperty(notes = "Identificador/Ticker")
    private String ticker;

    @ApiModelProperty(notes = "Nombre de la acción")
    private String name;

    @ApiModelProperty(notes = "Precio de la cotización en el momento de la consulta")
    private BigDecimal price;

}
