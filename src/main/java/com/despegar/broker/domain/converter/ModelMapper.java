package com.despegar.broker.domain.converter;

import java.util.List;
import java.util.stream.Collectors;

public interface ModelMapper {

    public <D> D map(Object source, Class<D> destinationType);

    public default <D> List<D> map(List<?> source, Class<D> destinationType) {
        return source.stream().map(s -> map(s, destinationType)).collect(Collectors.toList());
    }

}
