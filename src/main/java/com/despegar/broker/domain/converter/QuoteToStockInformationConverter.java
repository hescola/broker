package com.despegar.broker.domain.converter;

import org.springframework.stereotype.Component;

import com.despegar.broker.domain.valueObject.StockInformation;
import com.despegar.broker.domain.valueObject.StockQuote;

import lombok.val;

@Component
/**
 * TODO: mover a modelMapper interface
 * @author hescola
 *
 */
public class QuoteToStockInformationConverter {

    public StockInformation map(StockQuote stockQuote) {
        val stock = stockQuote.getStock();
        val stockInformation = StockInformation
            .builder()
            .name(stock.getName())
            .ticker(stock.getTicker())
            .price(stockQuote.getPrice())
            .build();

        return stockInformation;
    }

}
