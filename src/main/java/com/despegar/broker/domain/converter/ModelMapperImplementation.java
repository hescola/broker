package com.despegar.broker.domain.converter;

import org.modelmapper.config.Configuration.AccessLevel;
import org.springframework.stereotype.Component;

import com.despegar.broker.api.controller.dto.OrderItemDTO;
import com.despegar.broker.domain.valueObject.OrderItem;

@Component
public class ModelMapperImplementation implements ModelMapper {

    org.modelmapper.ModelMapper mapper = new org.modelmapper.ModelMapper();

    public ModelMapperImplementation() {

        mapper.getConfiguration()
            .setFieldMatchingEnabled(true)
            .setFieldAccessLevel(AccessLevel.PRIVATE);
        mapper.createTypeMap(OrderItemDTO.class, OrderItem.class)
            .setProvider(request ->
            {
                OrderItemDTO dto = OrderItemDTO.class.cast(request.getSource());
                return new OrderItem(dto.getTicker(), dto.getMoney());
            });
    }

    @Override
    public <D> D map(Object source, Class<D> destinationType) {
        return mapper.map(source, destinationType);
    }

}
