package com.despegar.broker.domain.validator;

import java.math.BigDecimal;
import java.util.List;

import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.exception.InvalidPurchaseOrderException;

public class PurchaseOrderValidator {

    public boolean validate(List<OrderItem> purchaseOrder) {

        boolean hasValidMoneyToExpend = purchaseOrder
            .stream()
            .allMatch(item -> item.getMoney() != null
                    && item.getMoney().compareTo(BigDecimal.ZERO) > 0);

        if (!hasValidMoneyToExpend) {
            throw new InvalidPurchaseOrderException("Money must be positive");
        }

        return true;
    }

}
