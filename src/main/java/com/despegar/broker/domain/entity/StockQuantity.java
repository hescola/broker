package com.despegar.broker.domain.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Wither;
/*
 * A pesar de que son Value Objects, los considero entities, porque son identificables por un Id
 */
@Builder
@EqualsAndHashCode
@Wither
@Value
public class StockQuantity {

    private String ticker;

    private int amount;

    /**
     * Crea una nueva copia con la diferencia entre amount}y [toSubtract]
     * 
     * @param stockAmount
     * @return
     */
    public StockQuantity subtract(StockQuantity stockAmount) {
        return withAmount(amount - stockAmount.getAmount());
    }

    public StockQuantity add(StockQuantity stockAmount) {
        return withAmount(amount + stockAmount.getAmount());
    }

    public static StockQuantity createWithZero(String ticker) {
        return new StockQuantity(ticker, 0);
    }

}
