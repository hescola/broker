package com.despegar.broker.domain.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;

/*
 * A pesar de que son Value Objects, los considero entities, porque son identificables por un Id
 */
@Builder
@Getter
@EqualsAndHashCode
@Value
public class Stock {

    private final String ticker;

    private final String name;
}
