package com.despegar.broker.domain.valueObject;

import java.math.BigDecimal;

import com.despegar.broker.domain.entity.StockQuantity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Value
public class QuotedOrderItem {

    private StockQuote quote;

    private OrderItem orderItem;

    private StockQuantity stockQuantity;

    private BigDecimal change;
}
