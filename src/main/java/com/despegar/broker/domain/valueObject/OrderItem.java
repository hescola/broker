package com.despegar.broker.domain.valueObject;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Builder
@JsonSerialize
@Value
@AllArgsConstructor
@Wither
public class OrderItem {

    private String ticker;

    private BigDecimal money;
}
