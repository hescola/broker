package com.despegar.broker.domain.valueObject;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@EqualsAndHashCode()
@Getter
@JsonSerialize
public class StockInformation {

    private String ticker;

    private String name;

    private BigDecimal price;

}
