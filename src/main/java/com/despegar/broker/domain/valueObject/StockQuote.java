package com.despegar.broker.domain.valueObject;

import java.math.BigDecimal;

import com.despegar.broker.domain.entity.Stock;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;
import lombok.experimental.Delegate;

@Builder
@Getter
@EqualsAndHashCode()
@Value
public class StockQuote {

    @Delegate
    private Stock stock;

    private BigDecimal price;

}
