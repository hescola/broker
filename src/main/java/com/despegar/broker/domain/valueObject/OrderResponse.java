package com.despegar.broker.domain.valueObject;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import groovy.transform.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Builder
@AllArgsConstructor
@JsonSerialize
@EqualsAndHashCode
@Value
public class OrderResponse {

    public static enum Status
    {
        SUCCESS,
        FAIL
    }

    private BigDecimal moneyNotExpended;

    private Status status;

    private String message;

    public static OrderResponse createFailWithReason(String reason) {
        return OrderResponse.builder()
            .status(Status.FAIL)
            .moneyNotExpended(null)
            .message(reason)
            .build();
    }

}
