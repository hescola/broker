package com.despegar.broker.domain.valueObject;

import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.loader.GenericPropertiesLoader;

@Component
public class StocksLoader extends GenericPropertiesLoader<Set<Stock>> {

    public StocksLoader() {
        this("/nombres.properties");
    }

    @Autowired
    public StocksLoader(@Value("${com.despegar.broker.loader.stocks}") String filePath) {
        super(filePath);
    }

    @Override
    protected Set<Stock> converReadedProperties(Properties prop) {
        return prop.entrySet()
            .stream()
            .map(stockEntry -> Stock.builder()
                .ticker((String) stockEntry.getKey())
                .name((String) stockEntry.getValue())
                .build())
            .collect(Collectors.toSet());
    }
}
