package com.despegar.broker.loader;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

import com.despegar.broker.domain.entity.StockQuantity;

public class StocksQuantityLoader extends GenericPropertiesLoader<Map<String, StockQuantity>> {

    public StocksQuantityLoader(String filePath) {
        super(filePath);
    }

    public StocksQuantityLoader() {
        // TODO: quitar esto de acá para los test
        this("/stock-inicial.properties");
    }

    @Override
    protected Map<String, StockQuantity> converReadedProperties(Properties prop) {
        return prop.entrySet()
            .stream()
            .map(this::convertToStockQuantity)

            .collect(Collectors.toMap(entry -> entry.getTicker(),
                    entry -> entry));
    }

    private StockQuantity convertToStockQuantity(Entry<Object, Object> entry) {
        StockQuantity stockAmount = StockQuantity.builder()
            .ticker((String) entry.getKey())
            .amount(Integer.valueOf((String) entry.getValue()))
            .build();
        return stockAmount;
    }
}
