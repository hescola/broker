package com.despegar.broker.loader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class GenericPropertiesLoader<T> {

    String filePath;

    public GenericPropertiesLoader(String filePath) {
        super();
        this.filePath = filePath;
    }

    protected abstract T converReadedProperties(Properties prop);

    public T load() {
        Properties prop = new Properties();
        InputStream input = null;
        T stocksToLoad2 = null;
        try {
            input = getClass().getResourceAsStream(filePath);
            prop.load(input);
            stocksToLoad2 = converReadedProperties(prop);
        } catch (IOException ex) {
            throw new PropertyLoaderException(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new PropertyLoaderException(e);
                }
            }
        }
        return stocksToLoad2;
    }

    public static class PropertyLoaderException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public PropertyLoaderException() {
            super();
        }

        public PropertyLoaderException(Throwable cause) {
            super(cause);
        }

    }
}
