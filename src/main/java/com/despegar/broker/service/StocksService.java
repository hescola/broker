package com.despegar.broker.service;

import java.util.List;

import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.domain.valueObject.OrderResponse;
import com.despegar.broker.domain.valueObject.StockInformation;
import com.despegar.broker.exception.InvalidPurchaseOrderException;
import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.exception.NotExistsStockException;

/*
 * Service de Acciones con las operaciones solicitadas en el ejercicio
 * 
 */
public interface StocksService {

    /**
     * Devuelve la información y la cotización actual de una Acción a través de su ticker
     * 
     * @param ticker
     * @return
     * @throws NotExistsStockException
     * @throws NotAvailableException
     */
    StockInformation getInformation(String ticker) throws NotExistsStockException, NotAvailableException;

    /**
     * En general hubiera hecho transaccional este metodo, pero por el tipo de operación a realizar
     * decidí que
     * la responsabilidad de asegurar threasSafe sea del repositorio. De esta forma me aseguro de
     * que
     * por implementar una nueva fucionalidad que haga uso del repositorio, ocurran resultados no
     * esperados.
     * Teniendo en cuenta lo anteriormente dicho, reconozco que se pierde flexibilidad
     * en el diseño realizandolo de esta forma, pero creo que a modo del ejercicio cumple con su
     * función que es asegurar que no haya solapamiento de datos ni error de transacción
     * 
     * @param purchaseOrder
     * @return
     * @throws InvalidPurchaseOrderException
     * @throws NotAvailableException
     */
    OrderResponse processPurchaseOrder(List<OrderItem> purchaseOrder)
            throws NotAvailableException;

}
