package com.despegar.broker.service;

import java.util.List;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.domain.valueObject.StockQuote;

/**
 * Serivicio para consulta de cotizaciones de acciones
 * 
 * @author Hernan Adriel Escola
 */
public interface QuoteService {

    List<StockQuote> getQuotes(List<Stock> stocks);

    StockQuote getQuote(Stock stock);

}
