package com.despegar.broker.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.despegar.broker.domain.converter.QuoteToStockInformationConverter;
import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.domain.validator.PurchaseOrderValidator;
import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.domain.valueObject.OrderResponse;
import com.despegar.broker.domain.valueObject.QuotedOrderItem;
import com.despegar.broker.domain.valueObject.StockInformation;
import com.despegar.broker.domain.valueObject.StockQuote;
import com.despegar.broker.domain.valueObject.OrderResponse.Status;
import com.despegar.broker.exception.InvalidPurchaseOrderException;
import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.exception.NotExistsStockException;
import com.despegar.broker.repository.StocksQuantityRepository;
import com.despegar.broker.repository.StocksRepository;
import com.despegar.broker.service.QuoteService;
import com.despegar.broker.service.StocksService;
import com.esotericsoftware.minlog.Log;

import lombok.val;

/**
 * No me gusta la idea de tener una sola clase que implementa la interfaz y mucho menos con el
 * "Impl" pero no quería depender de la abstracción
 * 
 * @author Hernan Adriel Escola
 */
@Singleton
@Service
public class StocksServiceImpl implements StocksService {

    public static final String ORDER_SUCCESSFULLY_PROCESSED = "Order successfully processed";

    private QuoteService quoteService;

    private StocksRepository stocksRepository;

    private StocksQuantityRepository StocksQuantityRepository;

    // TODO: hacerlo injectable
    private PurchaseOrderValidator validator = new PurchaseOrderValidator();

    @Autowired
    public StocksServiceImpl(QuoteService quoteService, StocksRepository stocksRepository,
            StocksQuantityRepository inventoryRespository) {
        super();
        this.quoteService = quoteService;
        this.stocksRepository = stocksRepository;
        this.StocksQuantityRepository = inventoryRespository;
    }

    @Override
    /**
     * trato el ticker como ID pero si viene en minúscula lo considero inválido. Como no hay nada
     * especificado al respecto delego esa responsabilidad a quien consuma el servicio
     */
    public StockInformation getInformation(String ticker) throws NotExistsStockException, NotAvailableException {
        Stock stock = stocksRepository.find(ticker).orElseThrow(() -> new NotExistsStockException(ticker));
        StockQuote quoteStock = quoteService.getQuote(stock);
        StockInformation stockInfo = new QuoteToStockInformationConverter().map(quoteStock);
        return stockInfo;
    }

    /**
     * Operacion de compra de acciones
     */
    @Override
    public OrderResponse processPurchaseOrder(List<OrderItem> purchaseOrder)
            throws InvalidPurchaseOrderException, NotAvailableException {

        try {
            validator.validate(purchaseOrder);
            List<QuotedOrderItem> quotedOrders = quoteOrder(purchaseOrder);
            List<StockQuantity> quantitiesToBuy = quotedOrders
                .stream()
                .map(quotedOrderItem -> quotedOrderItem.getStockQuantity())
                .collect(Collectors.toList());

            if (!StocksQuantityRepository.decreaseAllBy(quantitiesToBuy)) {
                throw new InvalidPurchaseOrderException("Not enough available quantity to fulfill the order");
            }
            auditTransaction(quotedOrders);
            return createSuccessfullOrderResponse(quotedOrders);
        } catch (InvalidPurchaseOrderException invalidPurchaseOrderExcepcion) {
            return createFailOrderResponse(invalidPurchaseOrderExcepcion);
        }
    }

    private OrderResponse createFailOrderResponse(InvalidPurchaseOrderException invalidPurchaseOrderExcepcion) {
        return OrderResponse.builder()
            .status(Status.FAIL)
            .message(invalidPurchaseOrderExcepcion.getMessage())
            .build();
    }

    private List<QuotedOrderItem> quoteOrder(List<OrderItem> purchaseOrder) {
        List<Stock> stocks = purchaseOrder.stream()
            .map(orderItem -> stocksRepository.find(orderItem.getTicker())
                .<InvalidPurchaseOrderException>orElseThrow(
                        () -> new InvalidPurchaseOrderException(orderItem.getTicker() + " Does not exists")))
            .collect(Collectors.toList());
        // Recollecto porque si hago todo de una sola corrida se
        // vuelve sincrónica la obtención de cada Cotización. De esta forma primero me aseguro de
        // preparar todos los Futures y luego obtengo el valor de cada uno

        Map<String, StockQuote> stockquotes = quoteService.getQuotes(stocks)
            .stream()
            .collect(Collectors.toMap(item -> item.getStock().getTicker(), item -> item));

        List<QuotedOrderItem> quotedOrders = combineToQuotedOrderItem(purchaseOrder, stockquotes);
        return quotedOrders;
    }

    /*
     */
    private void auditTransaction(List<QuotedOrderItem> quotedOrders) {
        Log.info("Order Succesfully Placed:");
        quotedOrders.forEach(item -> Log.info(item.toString()));
    }

    private OrderResponse createSuccessfullOrderResponse(List<QuotedOrderItem> quotedOrders) {
        BigDecimal moneyNotExpended = quotedOrders
            .stream()
            .map(order -> order.getChange())
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        return OrderResponse.builder()
            .moneyNotExpended(moneyNotExpended)
            .status(OrderResponse.Status.SUCCESS)
            .message(ORDER_SUCCESSFULLY_PROCESSED)
            .build();
    }

    private List<QuotedOrderItem> combineToQuotedOrderItem(List<OrderItem> purchaseOrder,
            Map<String, StockQuote> quotes) {
        return purchaseOrder.stream()
            .map(item -> combineToQuotedOrderItem(quotes, item))
            .collect(Collectors.toList());
    }

    private QuotedOrderItem combineToQuotedOrderItem(Map<String, StockQuote> quotes, OrderItem orderItem) {
        val quote = quotes.get(orderItem.getTicker());
        BigDecimal[] amountAndChange = orderItem.getMoney().divideAndRemainder(quote.getPrice());
        val amountToBuy = amountAndChange[0];
        val change = amountAndChange[1];
        StockQuantity stocksToBuy = StockQuantity.builder()
            .ticker(quote.getTicker())
            .amount(amountToBuy.intValue())
            .build();
        return new QuotedOrderItem(quote, orderItem, stocksToBuy, change);
    }

}
