package com.despegar.broker.service.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.domain.valueObject.StockQuote;
import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.external.api.client.QuoteRestApiClient;
import com.despegar.broker.service.QuoteService;

/*
 * 
 * Servicio que trata de garantinzar la obtención de de la contización en el menor tiempo posible
 * paralelizando los pedidos
 * A su vez el cliente que se utiliza reintenta en caso de error para intentar resolver la
 * transacción a pesar de obtener error
 * 
 * La Primera opción para resolver este probla fue usando caché (que fue lo que dije en el email),
 * pero ese mismo día se me ocurri'ó la solución utilizando Futures, que finalmente terminaron
 * siendo CompletableFutures para poder aprovechar ejecutar un callback cuando terminara.
 * 
 * Una vez implementada esta solución me convencí de que la solución a este problema debería ser con
 * un sistema reactivo.
 * Pensé en reescribirlo con RxJava, pero como se que hayq eu tener cuidado porque se pueden generar
 * leaks de memoria, preferí dejar esta solucíon y proponerlo como otra solución quizás mejor que
 * esta. La experiencia que tuve con rx fue desarrollando juegos, más que nada para UIs
 * y algunas partes de lógicas como inputs del jugador
 * 
 */
@Service
public class ExternalApiQuoteService implements QuoteService {

    // TODO: abstraer, generalizar e injectar
    private FuturesQuoteProvider quoteProvider;

    @Autowired
    public ExternalApiQuoteService(QuoteRestApiClient apiClient,
            @Value("${com.despegar.broker.service.impl.FuturesQuoteProvider.threadPoolSize}") int threadPoolSize) {
        super();
        this.quoteProvider = new FuturesQuoteProvider(apiClient, threadPoolSize);
    }

    /*
     * Obtiene una cotización sincronicamente
     */
    @Override
    public StockQuote getQuote(Stock stock) throws NotAvailableException {
        return getQuoteSync(quoteProvider.getFutureQuote(stock));
    }

    /**
     * Obtiene la cotizacion para los productos
     * Ejecuta los pedidos de cotizacion de forma paralela para optimizar la obtensión de los mismos
     */
    @Override
    public List<StockQuote> getQuotes(List<Stock> stocksToQuote) throws NotAvailableException {
        /*
         * Obtiene los Futures correspondientes a las cotizaciones de las acciones solicitadas
         * Luego de implementar esta solución, me di cuenta que podía haber usado alguna
         * implementación de reactive programming retornando observables, y solo exponiendo un solo
         * método getQuote que retornara un observable, que emitiera cuando estuviera lista la
         * cotizacion
         */
        List<Future<StockQuote>> futureQuotes = stocksToQuote.stream()
            .map(item -> quoteProvider.getFutureQuote(item))
            .collect(Collectors.toList());

        List<StockQuote> quotes = futureQuotes.stream()
            .map(this::getQuoteSync)
            .collect(Collectors.toList());
        return quotes;
    }

    private StockQuote getQuoteSync(Future<StockQuote> futureQuote) {
        try {
            return futureQuote.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new NotAvailableException(e);
        }
    }

}
