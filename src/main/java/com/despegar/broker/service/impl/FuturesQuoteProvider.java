package com.despegar.broker.service.impl;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.domain.valueObject.StockQuote;
import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.exception.NotFoundQuoteException;
import com.despegar.broker.external.api.client.QuoteRestApiClient;
import com.despegar.broker.external.dto.Quote;

/**
 * luego el futures correspondiente se mantenga cacheado hasta la resolición del mismo
 * Clase encargada de paralelizar la obtensión de las cotizaciones y almacenar un caché de los
 * futures que se encuentran executándose hasta ser resueltos.
 * TODO: Generizar para que sea mas un utilitario que ejecute un lambda y sean cacheado
 * 
 * @author Hernan Adriel Escola
 */
public class FuturesQuoteProvider {

    private ExecutorService requestQuoteExecutorService;

    private Map<Stock, Future<StockQuote>> cachedFutureStockQuotes = new ConcurrentHashMap<>();

    private QuoteRestApiClient apiClient;

    /**
     * La cantidad de threads configurada en la app fue tomando en cuenta que según las
     * observaciones de a 25 y 100 request de consultas paralelas , las terminaba resolviendo en el
     * mismo tiempo sin mejora aparente.
     * 
     * @param apiClient
     * @param threadPoolSize
     */
    public FuturesQuoteProvider(QuoteRestApiClient apiClient,
            int threadPoolSize) {
        super();
        this.apiClient = apiClient;
        requestQuoteExecutorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    /**
     * La idea es obtener siempre el posible ultimo valor de cotizacion. Si el sistema se
     * encuentra
     * buscando una cotizacion, cualquier consulta en paralelo que llegue recibe el mismo future
     * 
     * @param ticker
     * @return
     * @throws NotAvailableException
     */
    public Future<StockQuote> getFutureQuote(Stock stock) throws NotAvailableException {
        Future<StockQuote> futureStockQuote = cachedFutureStockQuotes
            .computeIfAbsent(stock, key -> createFutureStockQuote(key)
                .whenComplete((stockQuote, ex) -> cachedFutureStockQuotes.remove(stock)));
        return futureStockQuote;
    }

    /**
     * Crea un future de la cotizacion que se va a obtener en la request a la api del cotizador.
     * Utiliza el executor que tiene configurado un pool fijo de threads a utilizar. Según las
     * pruebas que hice, si se hacen más request en paralelo, igualmente el sistema no da a
     * basto
     * por lo tanto
     * no tiene sentido utilizar más threads.
     * 
     * @param stock
     * @return
     */
    private CompletableFuture<StockQuote> createFutureStockQuote(Stock stock) {
        String ticker = stock.getTicker();
        CompletableFuture<StockQuote> futureStockQuote =
                CompletableFuture
                    .supplyAsync(() -> createStockQuoteFromResponse(apiClient
                        .getQuote(ticker)
                        .orElseThrow(() -> new NotFoundQuoteException("Not found for " + ticker)), stock),
                            requestQuoteExecutorService);
        return futureStockQuote;
    }

    private StockQuote createStockQuoteFromResponse(Quote quoteResponse, Stock stock) {
        return StockQuote.builder()
            .stock(stock)
            .price(quoteResponse.getValue())
            .build();
    }

}
