package com.despegar.broker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Quote not found for given Ticker")
public class NotFoundQuoteException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotFoundQuoteException(String message) {
        super(message);
    }

}
