package com.despegar.broker.exception;

public class InvalidPurchaseOrderException extends RuntimeException {

    private static final long serialVersionUID = 5779979545166940390L;

    public InvalidPurchaseOrderException(String message) {
        super(message);
    }

}
