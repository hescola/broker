package com.despegar.broker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Excepcion arrojada cuando los servicios no están disponibles por realizar request a servicios
 * externos
 * 
 * @author Hernan Adriel Escola
 */
@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "The Service is currently unavailable. Try again.")
public class NotAvailableException extends RuntimeException {

    private static final long serialVersionUID = -6103995572037693973L;

    public NotAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAvailableException(Throwable cause) {
        super(cause);
    }

}
