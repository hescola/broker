package com.despegar.broker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Stock not found for given Ticker")
public class NotExistsStockException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotExistsStockException(String message) {
        super(message);
    }

}
