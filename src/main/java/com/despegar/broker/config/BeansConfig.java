package com.despegar.broker.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.despegar.broker.domain.valueObject.StocksLoader;
import com.despegar.broker.loader.StocksQuantityLoader;
import com.despegar.broker.repository.StocksQuantityRepository;
import com.despegar.broker.repository.StocksRepository;

@Configuration
public class BeansConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
            .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
            .addResourceLocations("classpath:/META-INF/resources/webjars/")
            .resourceChain(false);
    }

    @Bean
    public CommandLineRunner loadDataStocks(@Value("${com.despegar.broker.loader.stocks}") String fileToLoad,
            StocksRepository repository) {
        return (args) -> new StocksLoader(fileToLoad).load().forEach(item -> repository.save(item));
    }

    @Bean
    public CommandLineRunner loadDataStocksQuantity(
            @Value("${com.despegar.broker.loader.stocksQuantity}") String fileToLoad,
            StocksQuantityRepository repository) {
        return (args) -> new StocksQuantityLoader(fileToLoad).load()
            .forEach((ticker, quantity) -> repository.addOrIncreaseBy(quantity));
    }


}
