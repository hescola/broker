package com.despegar.broker.external.api.client;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.external.dto.Quote;

import lombok.extern.log4j.Log4j2;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

@Component
@Log4j2
/**
 * TODO: refactorear esta clase para que sea más limpia
 * 
 * @author hescola
 */
public class QuoteRestApiClient {

    private String basepath;

    private String endpoint;

    private RetryPolicy retryPolicy;

    @Autowired
    public QuoteRestApiClient(
            @Value("${com.despegar.broker.external.api.QuoteResApiClient.basepath}") String basepath,
            @Value("${com.despegar.broker.external.api.QuoteResApiClient.endpoint}") String endpoint) {
        this.endpoint = endpoint;
        this.basepath = basepath;
        initRetryPolicy();
    }

    public Optional<Quote> getQuote(String ticker) throws NotAvailableException {
        return Failsafe
            .with(retryPolicy)
            .onFailure(this::thowNotAvailableException)
            .get(() -> request(ticker));
    }

    private void initRetryPolicy() {
        this.retryPolicy = new RetryPolicy()
            .retryOn(Exception.class)
            .abortOn(NotAvailableException.class)
            .withDelay(1, TimeUnit.SECONDS)
            .withMaxRetries(6)
            .withMaxDuration(30, TimeUnit.SECONDS);
    }

    /**
     * TODO: refactor control de errores
     * 
     * @param ticker
     * @return
     */
    private Optional<Quote> request(String ticker) {
        try {
            Quote quoteResponse = webTargetFor(ticker).request(MediaType.APPLICATION_JSON).get(Quote.class);
            return Optional.of(quoteResponse);
        } catch (WebApplicationException error) {
            /*
             * Los codigos de error son 429,500,504, pero igualmente por las dudas voy a estar
             * reintentar en otros casos
             */
            log.debug("API QUOTE - Request status error for % " + error.getResponse().getLocation()
                    + ":" + error.getResponse().getStatusInfo());
            int responseStatus = error.getResponse().getStatus();
            if (responseStatus == HttpStatus.SC_NOT_FOUND) {
                return Optional.ofNullable(null);
            }
            throw error;
        } catch (ProcessingException error) {
            thowNotAvailableException(error);
        }
        return null;
    }

    public void thowNotAvailableException(Throwable error) {
        throw new NotAvailableException("The available", error);
    }

    private WebTarget webTargetFor(String ticker) {
        Client client = JerseyClientBuilder.newClient();
        WebTarget webTarget = client.target(basepath).path(endpoint).path(ticker);
        log.debug("Calling web service, URL [" + webTarget.getUri().toString() + "]");
        return webTarget;
    }

}
