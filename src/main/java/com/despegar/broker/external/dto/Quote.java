package com.despegar.broker.external.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder()
@JsonSerialize
@AllArgsConstructor
public class Quote implements Serializable {

    private static final long serialVersionUID = 1L;

    private String ticker;

    private BigDecimal value;

    public Quote() {

    }
}
