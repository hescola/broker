package com.despegar.broker.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.helper.DataLoaderTestHelper;
import com.despegar.broker.repository.StocksQuantityRepository;
import com.despegar.broker.repository.impl.InMemoryStocksQuantityRepository;

public class StocksQuantityRepositoryTest {

    private StocksQuantityRepository repository;

    @BeforeEach
    public void setup() {

    }

    @Nested
    public class GivenInMemoyRepositoryInitializedWithData {

        private Map<String, StockQuantity> initialStocksQuantity;

        @BeforeEach
        public void setup() {
            repository = new InMemoryStocksQuantityRepository();
            DataLoaderTestHelper dataLoader = new DataLoaderTestHelper();
            initialStocksQuantity = dataLoader.loadWithDefaultTestData(repository);
        }

        @Nested
        public class WhenDecreaseAllByAllAvailableStock {

            @BeforeEach
            public void setup() {

            }

            @Test
            public void ThenShouldBeZeroQuantity() {
                List<StockQuantity> quititiesToDecrement = initialStocksQuantity.entrySet()
                    .stream()
                    .map(entry -> StockQuantity.builder()
                        .ticker(entry.getKey())
                        .amount(initialStocksQuantity.get(entry.getKey()).getAmount())
                        .build())
                    .collect(Collectors.toList());

                repository.decreaseAllBy(quititiesToDecrement);

                quititiesToDecrement.forEach(stock ->
                {
                    assertEquals(0, repository.find(stock.getTicker()).get().getAmount());
                });

            }
        }

    }

}
