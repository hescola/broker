package com.despegar.broker;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.despegar.broker.api.controller.dto.OrderItemDTO;
import com.despegar.broker.api.controller.dto.OrderResponseDTO;
import com.despegar.broker.api.controller.dto.OrderResponseDTO.Status;
import com.despegar.broker.api.controller.dto.StockInformationDTO;
import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.external.api.client.QuoteRestApiClient;
import com.despegar.broker.helper.QuoteApiClientTestHelper;
import com.despegar.broker.loader.StocksQuantityLoader;
import com.despegar.broker.repository.impl.InMemoryStocksQuantityRepository;
import com.despegar.broker.util.StocksTestUtil;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = BrokerApplication.class)
@AutoConfigureWireMock(port = 9090)
public class IntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private QuoteRestApiClient quoteRestApiClient;

    private QuoteApiClientTestHelper apiClientHelper;

    private Map<String, StockQuantity> initialStocksQuantity = new HashMap<>();

    @BeforeEach
    public void setUp() throws InterruptedException {
        setUpQuoteApliClient();
        setUpStocksQuantityRepository();
    }

    public void setUpStocksQuantityRepository() {
        InMemoryStocksQuantityRepository inMemoryStocksStockRepository =
                new InMemoryStocksQuantityRepository();
        setUpInitialStocksQuantity(inMemoryStocksStockRepository);
    }

    public void setUpQuoteApliClient() {
        apiClientHelper = new QuoteApiClientTestHelper(quoteRestApiClient);
        apiClientHelper.mockDelayingResponse();
    }

    /**
     * Código repetido en el test del Service
     */
    private void setUpInitialStocksQuantity(InMemoryStocksQuantityRepository repository) {
        initialStocksQuantity = new StocksQuantityLoader().load();
        initialStocksQuantity.forEach((ticker, quantity) -> repository.addOrIncreaseBy(quantity));
    }

    @Test
    /**
     * TODO: agregar assert por comparion de objetos de StockInformationDTO.
     */
    public void givenValidTickerShouldResponseCorrectStockInformation() {
        String expectedTicker = "GOOGL";

        StockInformationDTO response =
                restTemplate.getForObject("/stocks/" + expectedTicker, StockInformationDTO.class);
        assertEquals(expectedTicker, response.getTicker());
    }

    @Test
    public void givenInvalidTickerShouldResponseWithError() {
        String expectedTicker = "INVALID";
        ResponseEntity<StockInformationDTO> response =
                restTemplate.getForEntity("/stocks/" + expectedTicker, StockInformationDTO.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void givenInvalidOrderToBuyShouldResponseWithFail() {
        List<OrderItem> invalid = StocksTestUtil.givenPurchaseOrderWithInvalidTickers();
        ResponseEntity<OrderResponseDTO> response =
                restTemplate.postForEntity("/stocks/buy", invalid, OrderResponseDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Status.FAIL, response.getBody().getStatus());
    }

    @Test
    public void givenValidOrderToBuyShouldResponseWithFail() {
        List<OrderItemDTO> valid = givenPurchaseOrderEqualsToInitialStock();

        BigDecimal expectedMonyNotExpended = BigDecimal.ZERO;
        ResponseEntity<OrderResponseDTO> response =
                restTemplate.postForEntity("/stocks/buy", valid, OrderResponseDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(com.despegar.broker.api.controller.dto.OrderResponseDTO.Status.SUCCESS,
                response.getBody().getStatus());
        assertEquals(expectedMonyNotExpended, response.getBody().getMoneyNotExpended());
    }

    private List<OrderItemDTO> givenPurchaseOrderEqualsToInitialStock() {
        return initialStocksQuantity.entrySet()
            .stream()
            .filter(item -> apiClientHelper.getExpectedPriceFor(item.getKey()) != null)
            .map(item -> OrderItemDTO.builder()
                .ticker(item.getKey())
                .money(apiClientHelper.getExpectedPriceFor(item.getKey())
                    .multiply(new BigDecimal(item.getValue().getAmount())))
                .build())
            .collect(Collectors.toList());
    }

}
