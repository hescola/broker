package com.despegar.broker.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.domain.valueObject.OrderResponse;
import com.despegar.broker.domain.valueObject.StockInformation;
import com.despegar.broker.domain.valueObject.StocksLoader;
import com.despegar.broker.exception.InvalidPurchaseOrderException;
import com.despegar.broker.exception.NotAvailableException;
import com.despegar.broker.exception.NotExistsStockException;
import com.despegar.broker.external.api.client.QuoteRestApiClient;
import com.despegar.broker.helper.DataLoaderTestHelper;
import com.despegar.broker.helper.QuoteApiClientTestHelper;
import com.despegar.broker.loader.StocksQuantityLoader;
import com.despegar.broker.repository.impl.InMemoryStocksQuantityRepository;
import com.despegar.broker.repository.impl.InMemoryStocksRepository;
import com.despegar.broker.service.impl.ExternalApiQuoteService;
import com.despegar.broker.service.impl.StocksServiceImpl;
import com.despegar.broker.util.StocksTestUtil;

import lombok.Getter;
import lombok.val;
import wiremock.com.google.common.collect.Lists;

/**
 * TODO: Mockear las dependencias porque actualmente está atravezando todas las capaz salvo la api
 * externa
 * que se encuentra mockeado el cliente
 * TODO: (REFACTOR) Extraer código reutilizable y pasar test a Jerarquía de clases anidadas del tipo
 * class->given->when->then
 * 
 * @author Hernan Adriel Escola
 */
public class StocksServiceTest {

    private StocksService stocksService;

    private QuoteService quoteService;

    private Map<String, StockQuantity> initialStocksQuantity = new HashMap<>();

    private QuoteRestApiClient quoteApiClient;

    private QuoteApiClientTestHelper quoteApiClientHelper;

    private DataLoaderTestHelper dataLoader;

    @BeforeEach
    public void setUp() {
        setUpQuoteService();
        setUpDataLoader();
        InMemoryStocksRepository inMemoryStocksRepository = new InMemoryStocksRepository();
        setupInitialStocks(inMemoryStocksRepository);
        InMemoryStocksQuantityRepository inMemoryStocksStockRepository =
                new InMemoryStocksQuantityRepository();
        setUpInitialStocksQuantity(inMemoryStocksStockRepository);
        stocksService =
                new StocksServiceImpl(mockQuoteSerice(), inMemoryStocksRepository, inMemoryStocksStockRepository);
    }

    private void setUpDataLoader() {
        dataLoader = new DataLoaderTestHelper();
    }

    private void setupInitialStocks(InMemoryStocksRepository inMemoryStocksRepository) {
        dataLoader.loadWithDefaultTestData(inMemoryStocksRepository);
    }

    private void setUpInitialStocksQuantity(InMemoryStocksQuantityRepository inMemoryStocksStockRepository) {
        initialStocksQuantity = dataLoader.loadWithDefaultTestData(inMemoryStocksStockRepository);
    }

    private void setUpQuoteService() {
        quoteApiClient = mockQuoteApiClient();
        quoteService = new ExternalApiQuoteService(quoteApiClient, 30);
    }

    private QuoteRestApiClient mockQuoteApiClient() {
        quoteApiClient = Mockito.mock(QuoteRestApiClient.class);
        quoteApiClientHelper = new QuoteApiClientTestHelper(quoteApiClient);
        quoteApiClientHelper.mockWithExpectedResponse();
        return quoteApiClient;
    }

    private QuoteService mockQuoteSerice() {
        return quoteService;
    }

    @Test
    public void shouldReturnValidStockInformation() throws NotExistsStockException, NotAvailableException {
        StockInformation expectedStockInformation = givenValidStock();
        String ticker = expectedStockInformation.getTicker();

        StockInformation stockInformation = stocksService.getInformation(ticker);

        assertEquals(expectedStockInformation.getPrice(), stockInformation.getPrice());
        assertEquals(expectedStockInformation, stockInformation);
    }

    @Test
    public void shouldThrowExceptionWhenStockNotExists() throws NotExistsStockException {
        // given
        String ticker = StocksTestUtil.getInvalidTicker();
        // when
        assertThrows(NotExistsStockException.class, () -> stocksService.getInformation(ticker));
    }

    @Test
    public void shouldThrowExceptionWhenPurchaseWithInvalidTicker() {
        List<OrderItem> purchaseOrder = StocksTestUtil.givenPurchaseOrderWithInvalidTickers();
        OrderResponse expectedOrderResponse = OrderResponse.createFailWithReason("Money must be positive");

        OrderResponse failResponse = stocksService.processPurchaseOrder(purchaseOrder);

        assertEquals(expectedOrderResponse, failResponse);
    }

    @Test
    public void shouldPlaceOrderAndSpendAllSuccessfully() throws InvalidPurchaseOrderException, NotAvailableException {
        // given
        List<OrderItem> purchaseOrder = givenPurchaseOrderEqualsToInitialStock();
        OrderResponse expectedOrderResponse = StocksTestUtil.getSucessfullResponseWithZeroMoneyNotExpended();
        // when
        OrderResponse orderResponse = stocksService.processPurchaseOrder(purchaseOrder);
        assertEquals(expectedOrderResponse, orderResponse);
    }

    @Test
    public void givenASecondOrderThatExceedCurrentStockShouldFail()
            throws InvalidPurchaseOrderException, NotAvailableException {
        // given
        List<OrderItem> purchaseOrderThatWillBuyAllStock = givenPurchaseOrderEqualsToInitialStock();
        stocksService.processPurchaseOrder(purchaseOrderThatWillBuyAllStock);
        OrderResponse expectedOrderResponse =
                OrderResponse.createFailWithReason("Not enough available quantity to fulfill the order");
        // when
        List<OrderItem> purchaseOrderThatCantBeFulfilled =
                Lists.newArrayList(givenPurchaseOrderEqualsToInitialStock().stream().findAny().get());
        OrderResponse failResponse = stocksService.processPurchaseOrder(purchaseOrderThatCantBeFulfilled);
        // then
        assertEquals(expectedOrderResponse, failResponse);
    }

    @Test
    public void givenAnOrderShouldReturnCorrectMoneyNotExpended()
            throws InvalidPurchaseOrderException, NotAvailableException {
        // given
        val expectedChange = new BigDecimal("34.22");
        val purchaseOrder = givenPurchaseOrderWithExpectedMoneyNotExpended(expectedChange);
        val expectedOrderResponse = StocksTestUtil.getSucessfullResponseWithMoney(expectedChange);
        // when
        val orderResponse = stocksService.processPurchaseOrder(purchaseOrder);
        // then
        assertEquals(expectedOrderResponse, orderResponse);
    }

    private StockInformation givenValidStock() throws NotExistsStockException, NotAvailableException {
        StockInformation expectedStockInformation;
        expectedStockInformation = createValidStockInformation();
        // given(stockRepository.find(expectedStockInformation.getTicker()))
        // .willReturn(expectedStockInformation.getStock());
        return expectedStockInformation;
    }

    private List<OrderItem> givenPurchaseOrderWithExpectedMoneyNotExpended(BigDecimal expectedChange) {
        val purchaseOrderThatWillBuyAllStock = givenPurchaseOrderEqualsToInitialStock();
        val itemToAddMoney = purchaseOrderThatWillBuyAllStock.get(0);
        purchaseOrderThatWillBuyAllStock.set(0,
                itemToAddMoney.withMoney(itemToAddMoney.getMoney().add(expectedChange)));
        return purchaseOrderThatWillBuyAllStock;
    }

    private List<OrderItem> givenPurchaseOrderEqualsToInitialStock() {
        return StocksTestUtil.givenPurchaseOrderEqualsToInitialStock(initialStocksQuantity,
                quoteApiClientHelper.getExpectedPrices());
    }

    private StockInformation createValidStockInformation() {
        String ticker = "GOOGL";
        String expectedName = "Google";

        StockInformation expectedStockInformation = StockInformation.builder()
            .price(quoteApiClientHelper.getExpectedPriceFor(ticker))
            .name(expectedName)
            .ticker(ticker)
            .build();
        return expectedStockInformation;
    }

}
