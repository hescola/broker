package com.despegar.broker.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.domain.valueObject.StockQuote;
import com.despegar.broker.domain.valueObject.StocksLoader;
import com.despegar.broker.exception.NotFoundQuoteException;
import com.despegar.broker.external.api.client.QuoteRestApiClient;
import com.despegar.broker.external.dto.Quote;
import com.despegar.broker.helper.QuoteApiClientTestHelper;
import com.despegar.broker.service.impl.ExternalApiQuoteService;

import io.reactivex.Single;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class QuoteApiServiceTest {

    private QuoteService quoteService;

    private QuoteRestApiClient apiClient;

    private Random random;

    private Set<Stock> stocksSet;

    private QuoteApiClientTestHelper quoteApiClientHelper;

    @BeforeEach
    public void setUp() {
        random = new Random(1);
        stocksSet = new StocksLoader().load();
        setUpQuoteService();
    }

    public void setUpQuoteService() {
        apiClient = Mockito.mock(QuoteRestApiClient.class);
        quoteApiClientHelper = new QuoteApiClientTestHelper(apiClient);
        mockQuoteRestApiClient();
        quoteService = new ExternalApiQuoteService(apiClient, 20);
    }

    private void mockQuoteRestApiClient() {
        new QuoteApiClientTestHelper(apiClient).mockWithExpectedResponse();
    }

    @Test
    public void givenListOfTickersShouldReturnCorretQuote() {
        List<Stock> stocks = new StocksLoader().load().stream().collect(Collectors.toList());
        List<StockQuote> quote = quoteService.getQuotes(stocks);

        quote.forEach(item ->
        {
            assertEquals(quoteApiClientHelper.getExpectedPriceFor(item.getTicker()), item.getPrice());
            assertTrue(stocks.contains(item.getStock()));
        });
    }

    
    /**
     * Procesa 10 pedidos paralelos de listad de acciones a cotizar
     */
    @Test
    public void givenConcurrentsRequestShouldNotThrowException() {
        new QuoteApiClientTestHelper(apiClient).mockDelayingResponse();
        IntStream.range(0, 10)
            .parallel()
            .forEach(interation ->
            {
                List<StockQuote> quote = quoteService.getQuotes(getRandomElements(stocksSet));
                quote.stream().forEach(item -> log.debug(item));
            });
    }

    /**
     * TODO: mover a utilitario
     * 
     * @param stocksSet
     * @return
     */
    private List<Stock> getRandomElements(Set<Stock> stocksSet) {
        return stocksSet.stream()
            .filter(stock -> random.ints(1, 3).findFirst().getAsInt() != 1)
            .collect(Collectors.toList());
    }

    /**
     * Estos eran pruebas que estaba haciendo con los RxJava para ver si era factible que anduviera
     * todo bien.
     * Si tuviera que rediseñar la aplicación, la haría reactiva sin dudar
     */
    public void testObservable() {
        ExecutorService requestQuoteExecutorService = Executors.newFixedThreadPool(50);

        List<Single<Quote>> flowablesList = stocksSet.stream().map(stock ->
        {
            CompletableFuture<Quote> futureStockQuote = CompletableFuture
                .supplyAsync(() ->
                {
                    String ticker = stock.getTicker();
                    log.debug("Yendo  buscar data for" + ticker);
                    try {
                        Thread.sleep(RandomUtils.nextLong(0l, 5000l));
                    } catch (InterruptedException e) {
                        log.error(e);
                    }
                    log.debug("obtenido" + ticker);
                    return apiClient.getQuote(stock.getTicker())
                        .<NotFoundQuoteException>orElseThrow(
                                () -> new NotFoundQuoteException("Not found for " + ticker));
                }, requestQuoteExecutorService);
            return Single.fromFuture(futureStockQuote);
        }).collect(Collectors.toList());
        flowablesList.stream();

        // flowablesList
        // Subject.fromCallable((a)->a);
        // Observable.f
        // Single.zip(flowablesList,item -> item).toObservable().flatMap(a-> );

        // flowablesList.stream().map(val ->
        // {
        // log.debug("Iterando asincronicamente " + val.count().cache());
        // return val;
        // })
        // .map(item -> item.blockingSingle())
        // .map(val ->
        // {
        // log.debug("Resuelto " + val);
        // return val;
        // })
        // .collect(Collectors.toList());
        // //
        // Flowable<QuoteResponse> a = observable.toFlowable();
        // Disposable dispoable = a.subscribe(item -> log.debug("primero " + item.getValue()));
        // log.debug("esperando 1");
        // a.blockingFirst();
        // dispoable.dispose();
        // log.debug("preparando segundo 1");
        // Flowable<QuoteResponse> b = observable.toFlowable();
        // b.subscribe(item -> log.debug("segundo " + item.getValue()));
        //
        // List<Flowable<QuoteResponse>> observables = Lists.newArrayList(a, b);
        // log.debug("Terminado ");
        // // Observable<Observable<?>> obsObs = Observable.(observables);
        //
        // observables.stream().forEach(item -> log.debug("segundo " + item.blockingSingle()));
        // observable.test();
    }

}
