package com.despegar.broker.external.api.client;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.despegar.broker.external.dto.Quote;
import com.despegar.broker.helper.QuoteApiClientTestHelper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.stubbing.Scenario;

public class QuoteRestApiClientTest {

    private static final String FAILSAFE_SCENARIO = "Failsafe";

    private QuoteRestApiClient quoteRestApiClient;

    int port = 2345;

    private String endpoint = "stocks";

    private WireMockServer wireMockServer;

    private static Map<String, BigDecimal> quotesMap;

    @BeforeEach
    void setup() throws Exception {
        setupWireMock();
        setupDefaultStubForApi();
    }

    @AfterEach
    void teardown() {
        wireMockServer.stop();
    }

    private void setupDefaultStubForApi() {
        quoteRestApiClient = new QuoteRestApiClient(getBasePath(), endpoint);
        quotesMap = QuoteApiClientTestHelper.DEFAULT_EXPECTED_PRICES;
        stubFor(get(urlPathMatching("/stocks/.*"))
            .willReturn(aResponse()
                .withStatus(404)
                .withHeader("Content-Type", "application/json")));
    }

    private void setupWireMock() {
        wireMockServer = new WireMockServer(port);
        wireMockServer.start();
        configureFor(wireMockServer.port());
    }

    private String getBasePath() {
        return "http://" + wireMockServer.getOptions().bindAddress() + ":" + port;
    }

    @Test
    void givenInvalidTickerShouldReturnEmptyOptional() {
        String ticker = "ABC";
        Optional<Quote> quote = quoteRestApiClient.getQuote(ticker);
        assertFalse(quote.isPresent());
    }

    @Test
    void givenValidTickerWhenRequestQuoteThenReturnExpected() {
        String ticker = getValidTicker();
        setupValidTickerStub(ticker);
        Quote expectedQuote = createExpectedQuoteResponse(ticker);

        Optional<Quote> quote = quoteRestApiClient.getQuote(ticker);

        assertTrue(quote.isPresent());
        assertEquals(expectedQuote, quote.get());
    }

    @Test
    void givenValidTickerWhenRequestFailThenRetryAndSuccess() {
        String ticker = getValidTicker();
        setupFaultyScenario(ticker);
        Quote expectedQuote = createExpectedQuoteResponse(ticker);

        Optional<Quote> quote = quoteRestApiClient.getQuote(ticker);

        assertTrue(quote.isPresent());
        assertEquals(expectedQuote, quote.get());
    }

    private Quote createExpectedQuoteResponse(String ticker) {
        Quote expectedQuote = Quote.builder()
            .ticker(ticker)
            .value(quotesMap.get(ticker))
            .build();
        return expectedQuote;
    }

    private String getValidTicker() {
        return quotesMap.keySet().stream().findAny().get();
    }

    private void setupValidTickerStub(String ticker) {
        stubFor(get(urlPathMatching("/" + endpoint + "/" + ticker))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("{\"ticker\":\"" + ticker + "\",\"value\":" + quotesMap.get(ticker) + "}")));
    }

    void setupFaultyScenario(String ticker) {
        String FISRT_FAIL_STATE = "FIRST_ERROR";
        String SECOND_FAIL_STATE = "SECOND_ERROR";
        String THIRD_FAIL_STATE = "THIRD_ERROR";

        stubFor(get(urlPathMatching("/" + endpoint + "/" + ticker))
            .inScenario(FAILSAFE_SCENARIO)
            .whenScenarioStateIs(Scenario.STARTED)
            .willReturn(aResponse()
                .withStatus(500)
                .withHeader("Content-Type", "application/json"))
            .willSetStateTo(FISRT_FAIL_STATE));

        stubFor(get(urlPathMatching("/" + endpoint + "/" + ticker))
            .inScenario(FAILSAFE_SCENARIO)
            .whenScenarioStateIs(FISRT_FAIL_STATE)
            .willReturn(aResponse()
                .withStatus(429)
                .withHeader("Content-Type", "application/json"))
            .willSetStateTo(SECOND_FAIL_STATE));

        stubFor(get(urlPathMatching("/" + endpoint + "/" + ticker))
            .inScenario(FAILSAFE_SCENARIO)
            .whenScenarioStateIs(SECOND_FAIL_STATE)
            .willReturn(aResponse()
                .withStatus(504)
                .withHeader("Content-Type", "application/json")
                .withBody("{\"ticker\":\"" + ticker + "\",\"value\":" + quotesMap.get(ticker) + "}"))
            .willSetStateTo(THIRD_FAIL_STATE));

        stubFor(get(urlPathMatching("/" + endpoint + "/" + ticker))
            .inScenario(FAILSAFE_SCENARIO)
            .whenScenarioStateIs(THIRD_FAIL_STATE)
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("{\"ticker\":\"" + ticker + "\",\"value\":" + quotesMap.get(ticker) + "}"))
            .willSetStateTo(Scenario.STARTED));
    }

}
