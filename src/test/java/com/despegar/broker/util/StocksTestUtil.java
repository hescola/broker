package com.despegar.broker.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.domain.valueObject.OrderItem;
import com.despegar.broker.domain.valueObject.OrderResponse;
import com.despegar.broker.domain.valueObject.OrderResponse.Status;
import com.despegar.broker.exception.NotExistsStockException;
import com.despegar.broker.service.impl.StocksServiceImpl;

import wiremock.com.google.common.collect.Lists;

public class StocksTestUtil {

    public static List<OrderItem> givenPurchaseOrderWithInvalidTickers() {
        return Lists.newArrayList(OrderItem.builder()
            .ticker(getInvalidTicker())
            .build());
    }

    public static String getInvalidTicker() throws NotExistsStockException {
        String notExistingTicker = "invalid";
        return notExistingTicker;
    }

    public static OrderResponse getSucessfullResponseWithMoney(BigDecimal money) {
        return OrderResponse.builder()
            .status(Status.SUCCESS)
            // TODO: Pasar a properties el mensaje y leerlo de ahí en los tests
            .message(StocksServiceImpl.ORDER_SUCCESSFULLY_PROCESSED)
            .moneyNotExpended(money)
            .build();
    }

    public static OrderResponse getSucessfullResponseWithZeroMoneyNotExpended() {
        return StocksTestUtil.getSucessfullResponseWithMoney(BigDecimal.ZERO);
    }

    /**
     * Mover a utilitario de OrderItem
     * 
     * @param initialStocksQuantity
     * @param prices
     * @return
     */
    public static List<OrderItem> givenPurchaseOrderEqualsToInitialStock(
            Map<String, StockQuantity> initialStocksQuantity, Map<String, BigDecimal> prices) {
        return initialStocksQuantity.entrySet()
            .stream()
            .filter(item -> prices.containsKey(item.getKey()))
            .map(item -> OrderItem.builder()
                .ticker(item.getKey())
                .money(prices.get(item.getKey())
                    .multiply(new BigDecimal(item.getValue().getAmount())))
                .build())
            .collect(Collectors.toList());
    }
}
