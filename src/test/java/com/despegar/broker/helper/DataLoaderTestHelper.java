package com.despegar.broker.helper;

import java.util.Map;
import java.util.Set;

import com.despegar.broker.domain.entity.Stock;
import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.domain.valueObject.StocksLoader;
import com.despegar.broker.loader.StocksQuantityLoader;
import com.despegar.broker.repository.StocksQuantityRepository;
import com.despegar.broker.repository.StocksRepository;

import lombok.Getter;

/**
 * Helper para la carga de datos en los repositorios
 * TODO: leer directamente del application.properties
 * No uso springRunner porque hace que los test se vuelvan más lentos y salvo para integración,
 * prefiero evitarlo.
 * TODO: refactoear código para convertir en métodos de instancia y no estáticos
 * 
 * @author Hernan Adriel Escola
 */
public class DataLoaderTestHelper {

    @Getter
    private final Set<Stock> stockData = readStockData();

    @Getter
    private final Map<String, StockQuantity> stockQuantityData = readStockQuantityData();

    public final String STOCKS_DATA_FILEPATH = "/data/nombres-test.properties";

    public final String STOCKS_QUANTITY_DATA_FILEPATH = "/data/stock-inicial-test.properties";

    public DataLoaderTestHelper() {}

    public Map<String, StockQuantity> loadWithDefaultTestData(StocksQuantityRepository repository) {
        stockQuantityData.forEach((ticker, quantity) -> repository.addOrIncreaseBy(quantity));
        return stockQuantityData;
    }

    public Set<Stock> loadWithDefaultTestData(StocksRepository repository) {
        stockData.forEach(item -> repository.save(item));
        return stockData;
    }

    private Set<Stock> readStockData() {
        return new StocksLoader().load();
    }

    private Map<String, StockQuantity> readStockQuantityData() {
        return new StocksQuantityLoader().load();
    }
}
