package com.despegar.broker.helper;

import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.RandomUtils;
import org.mockito.ArgumentMatchers;

import com.despegar.broker.domain.entity.StockQuantity;
import com.despegar.broker.external.api.client.QuoteRestApiClient;
import com.despegar.broker.external.dto.Quote;

import lombok.Getter;

/**
 * Helper para mockear la Api del Cotizador
 * 
 * @author Hernan Adriel Escola
 */
public class QuoteApiClientTestHelper {

    /**
     * TODO: mover a un helper propio
     */
    public final static Map<String, BigDecimal> DEFAULT_EXPECTED_PRICES = new HashMap<>();
    static {
        DEFAULT_EXPECTED_PRICES.put("AMZN", new BigDecimal(100));
        DEFAULT_EXPECTED_PRICES.put("YHOO", new BigDecimal(200));
        DEFAULT_EXPECTED_PRICES.put("MSFT", new BigDecimal(300));
        DEFAULT_EXPECTED_PRICES.put("FB", new BigDecimal(400));
        DEFAULT_EXPECTED_PRICES.put("GOOGL", new BigDecimal(500));
        DEFAULT_EXPECTED_PRICES.put("GDDY", new BigDecimal(600));
        DEFAULT_EXPECTED_PRICES.put("BABA", new BigDecimal(700));
        DEFAULT_EXPECTED_PRICES.put("EBAY", new BigDecimal(800));
        DEFAULT_EXPECTED_PRICES.put("AAPL", new BigDecimal(900));
    }

    private QuoteRestApiClient quoteApiClient;
    
    @Getter // debería ser immutable
    private Map<String, BigDecimal> expectedPrices;

    public QuoteApiClientTestHelper(QuoteRestApiClient quoteApiClient) {
        this.quoteApiClient = quoteApiClient;
        this.expectedPrices = DEFAULT_EXPECTED_PRICES;
    }

    public QuoteApiClientTestHelper(QuoteRestApiClient quoteApiClient, Map<String, BigDecimal> expectedPrices) {
        this(quoteApiClient);
        this.expectedPrices = expectedPrices;
    }

    public BigDecimal getExpectedPriceFor(String ticker) {
        return expectedPrices.get(ticker);
    }
    
    public static BigDecimal getDefaultExpectedPriceFor(String ticker) {
        return DEFAULT_EXPECTED_PRICES.get(ticker);
    }

    public QuoteRestApiClient mockWithExpectedResponse() {
        given(quoteApiClient.getQuote(ArgumentMatchers.any(String.class)))
            .will((arguments) -> getResponseHandlerWithExpectedValues().apply(arguments.getArgument(0)));
        return quoteApiClient;
    }

    public QuoteRestApiClient mockResponseWithHandler(Function<String, Optional<Quote>> responseHandler) {
        given(quoteApiClient.getQuote(ArgumentMatchers.any(String.class)))
            .will((arguments) -> responseHandler.apply(arguments.getArgument(0)));
        return quoteApiClient;
    }

    public QuoteRestApiClient mockDelayingResponse() {
        Function<String, Optional<Quote>> responseHandler = (ticker) ->
        {
            try {
                Thread.sleep(RandomUtils.nextLong(0l, 1000l));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return getResponseHandlerWithExpectedValues().apply(ticker);
        };
        return mockResponseWithHandler(responseHandler);
    }

    private Function<String, Optional<Quote>> getResponseHandlerWithExpectedValues() {
        return (ticker) -> Optional.of(Quote.builder()
            .ticker(ticker)
            .value(getExpectedPriceFor(ticker))
            .build());
    }

}
