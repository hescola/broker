# Broker

Despegar - Evaluación Técnica Hernán Adriel Escola
Broker Service

Para consultar la API cotizador, implementé una solución utilizando CompletableFutures con una caché que se invalida al resolver la request de la cotización.
Me hubiera encantado hacer la app reactiva. Hice pruebas y creo que podría haberla hecho pero me hubiera llevado más tiempo e invertí unas cuantas horas investigando sobre Ractive Programming y Sistemas Reactivos. Ya había tenido un acercamiento a RX por los juegos que hago en Unity utilizando UniRX. En otra oportunidad lo haré.
Utilicé la librería FailSafe para reintentar en caso de errores y para la transacción de las compras utilicé un lock para asegurar que sea Thread Safe

## Tecnologías y Herramientas
* Java 8
* Lombok
* Maven
* Jersey - Cliente API
* Spring Rest, Spring Boot, Spring Web
* Failsafe - Reintentos
* Swagger - Generación de documentación de la API. Swagger-UI disponible para visualizar las especificaciones de la API
* JUnit5

* Mockito - Test
* WireMock - Test
* Jacoco - Reporte de cobertura de tests

## Instrucciones

Para correr el proyecto localmente es necesario utilizar Maven para correr los test y buildear

### Requisitos

Antes de empezar es necesario tener instalado:

- JDK 1.8 con la variable de entorno JAVA_HOME configurada correctamente
- Maven 3.x

## Configuración

Los parámetros configurables de la aplicación se encuentran en /main/resources/application.properties
en este archivo se encuentran las siguientes properties:

### Path base de la Api del cotizador
```
com.despegar.broker.external.api.QuoteResApiClient.basepath=http://0.0.0.0:8080 
```
###  Endpoint de la API del Cotizador
```
com.despegar.broker.external.api.QuoteResApiClient.endpoint=stocks
```
### Archivos de configuración con los valores iniciales de la aplicación (Directorios relativos a /main/resources)
```
com.despegar.broker.loader.stocks=/nombres.properties
com.despegar.broker.loader.stocksQuantity=/stock-inicial.properties
```

# Build

Primero que nada posicionarse en el root del proyecto. Si se necesita cambiar alguna propertie de la aplicación, hacerlo antes de seguir con este proceso.

###Para generar el JAR

```
mvn clean install
```

Este comando correrán los test, se generará el reporte de cobertura y se buildeará la aplicación generando el archivo JAR en  /target/broker-1.0.0.jar


### Ejecutar la aplicación
Con el siguiente comando se ejecutará la aplicación con el puerto default 9000 (también configurable en las properties)

```
java -jar target/broker-1.0.0.jar
```
Si se desea ocupar otro puerto, por ejemplo el 9000, ejecutar:

```
java -jar target/broker-1.0.0.jar --server.port=9000
```


Una vez inciado, si todo está en orden la aplicación estará corriendo en puerto proporcionado con la siguiente URI

http://localhost:9000/broker

Tener en cuenta que por default intentará conectarse a la api del cotizador en http://localhost:8080/stocks


## Documentación de la API - Swagger

La documentación de la API se encuentra en 

http://localhost:9000/broker/swagger-ui.html

En la Documentación se encuentran las respuestas en caso de errorla cual contiene las dos operaciones solicitadas:

GET - /broker/stocks/{ticker}

```
curl -X GET "http://localhost:9000/broker/stocks/{ticker}"

RESPONSE:
{"ticker":"AMZN","name":"Amazon","price":345.4064674299327}
```

POST - /broker/stocks/buy

```
curl -X POST "http://localhost:9000/broker/stocks/buy" -H "Content-Type: application/json" -d "[ { \"money\": 500.34, \"ticker\": \"AMZN\" },{ \"money\": 235.4125, \"ticker\": \"BABA\" }]"

RESPONSE:
{
  "moneyNotExpended": 176.50004321767145,
  "status": "SUCCESS",
  "message": "Order successfully processed"
}
```


### Test Coverage

En la Ruta /target/jacoco-ut/index.html se puede ver el informe de la cobertura de los Test
La aplicación se encuentra cubierta por los test en un 90%


## Autor

* **Hernán Adriel Escola** - [HernanEscola](https://github.com/HernanEscola)

